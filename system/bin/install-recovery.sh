#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:34383182:892a9c32a9d684ff2ec7898dc367b983da22c86b; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:27743562:f27a040f5b40a7d8847003b946f0736cbbe0fb44 EMMC:/dev/block/bootdevice/by-name/recovery 892a9c32a9d684ff2ec7898dc367b983da22c86b 34383182 f27a040f5b40a7d8847003b946f0736cbbe0fb44:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
