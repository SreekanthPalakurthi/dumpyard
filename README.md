# whyred-user 9 PKQ1.180904.001 20.5.28 release-keys
- manufacturer: xiaomi
- platform: sdm660
- codename: whyred
- flavor: whyred-user
- release: 9
- id: PKQ1.180904.001
- incremental: 20.5.28
- tags: release-keys
- fingerprint: xiaomi/whyred/whyred:9/PKQ1.180904.001/20.5.28:user/release-keys
- brand: xiaomi
